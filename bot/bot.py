import time
import inspect
import traceback
import asyncio
import discord

from discord import utils
from discord.object import Object

from bot.config import Config
from bot.utils import load_file, extract_user_id, write_file

from .exceptions import CommandError
from .constants import DISCORD_MSG_CHAR_LIMIT

class Response(object):
  def __init__(self, content, reply=False, delete_after=0):
    self.content = content
    self.reply = reply
    self.delete_after = delete_after


class Bot(discord.Client):
  def __init__(self, config_file='config/options.txt'):
    super().__init__()

    self.config = Config(config_file)

    self.blacklist = set(map(int, load_file(self.config.blacklist_file)))
    self.whitelist = set(map(int, load_file(self.config.whitelist_file)))

    self.last_np_msg = None

  def run(self):
    return super().run(self.config.username, self.config.password)

  @asyncio.coroutine
  def on_ready(self):
    print('Connected!\n')
    print('Username: %s' % self.user.name)
    print('Bot ID: %s' % self.user.id)
    print('Owner ID: %s' % self.config.owner_id)

    print("Command prefix is %s" % self.config.command_prefix)
    print("Whitelist check is %s" % ['disabled', 'enabled'][self.config.white_list_check])


    if self.servers:
      print('--Server List--')
      [print(s) for s in self.servers]
    else:
      print("No servers have been joined yet.")

    print()

  def _fixg(self, x, dp=2):
    return ('{:.%sf}' % dp).format(x).rstrip('0').rstrip('.')

  #####################
  # START OF COMMANDS #
  #####################

  @asyncio.coroutine
  def handle_help(self):
    """
    Usage: {command_prefix}help
    Prints a help message
    """
    helpmsg = "**Commands**\n```"
    commands = []

    # TODO: Get this to format nicely
    for att in dir(self):
      if att.startswith('handle_') and att != 'handle_help':
        command_name = att.replace('handle_', '').lower()
        commands.append("{}{}".format(self.config.command_prefix, command_name))

    helpmsg += ", ".join(commands)

    return Response(helpmsg, reply=True, delete_after=60)


  @asyncio.coroutine
  def handle_id(self, author):
    """
    Usage: {command_prefix}id
    Tells the user their id.
    """
    return Response('your id is `%s`' % author.id, reply=True)


  @asyncio.coroutine
  def handle_joinserver(self, message, server_link):
    """
    Usage {command_prefix}joinserver [Server Link]
    Asks the bot to join a server. [todo: add info about if it breaks or whatever]
    """
    try:
      if message.author.id == self.config.owner_id:
        yield from self.accept_invite(server_link)

    except:
      raise CommandError('Invalid URL provided:\n{}\n'.format(server_link))


  @asyncio.coroutine
  def handle_clean(self, message, author, amount):
    """
    Usage {command_prefix}clean amount
    Removes [amount] messages the bot has posted in chat.
    """
    pass

  @asyncio.coroutine
  def handle_topic(self, message, channel, new_topic, channel_id=None):
    """
    Usage {command_prefix}topic new_topic [channel_id]
    Changes the current channel topic to [new_topic], or the channel whose id is supplied.
    """
    if channel_id:
      channel = self.get_channel(channel_id)

    try:
      yield from self.edit_channel(channel, name = channel.name, position = channel.position, topic = new_topic)
      return Response('Set the topic for #%s to \"%s\"' % (channel.name, new_topic))

    except Exception as e:
      raise CommandError(e)


  ##################
  # MESSAGE READER #
  ##################

  @asyncio.coroutine
  def on_message(self, message):
    if message.author == self.user:
      if message.content.startswith(self.config.command_prefix):
        print("Ignoring command from myself (%s)" % message.content)
      return

    if message.channel.is_private:
      yield from self.send_message(message.channel, 'You cannot use this bot in private messages.')
      return

    message_content = message.content.strip()
    if not message_content.startswith(self.config.command_prefix):
      return

    command, *args = message_content.split()
    command = command[len(self.config.command_prefix):].lower().strip()

    handler = getattr(self, 'handle_%s' % command, None)
    if not handler:
      return


    if int(message.author.id) in self.blacklist and message.author.id != self.config.owner_id:
      print("[Blacklisted] {0.id}/{0.name} ({1})".format(message.author, message_content))
      return

    elif self.config.white_list_check and int(message.author.id) not in self.whitelist and message.author.id != self.config.owner_id:
      print("[Not whitelisted] {0.id}/{0.name} ({1})".format(message.author, message_content))
      return

    else:
      print("[Command] {0.id}/{0.name} ({1})".format(message.author, message_content))


    argspec = inspect.signature(handler)
    params = argspec.parameters.copy()

    # noinspection PyBroadException
    try:
      handler_kwargs = {}
      if params.pop('message', None):
        handler_kwargs['message'] = message

      if params.pop('channel', None):
        handler_kwargs['channel'] = message.channel

      if params.pop('author', None):
        handler_kwargs['author'] = message.author

      args_expected = []
      for key, param in list(params.items()):
        doc_key = '[%s=%s]' % (key, param.default) if param.default is not inspect.Parameter.empty else key
        args_expected.append(doc_key)

        if not args and param.default is not inspect.Parameter.empty:
          params.pop(key)
          continue

        if args:
          arg_value = args.pop(0)
          handler_kwargs[key] = arg_value
          params.pop(key)

      if params:
        docs = getattr(handler, '__doc__', None)
        if not docs:
          docs = 'Usage: {}{} {}'.format(
            self.config.command_prefix,
            command,
            ' '.join(args_expected)
          )

        docs = '\n'.join(l.strip() for l in docs.split('\n'))
        yield from self.send_message(
          message.channel,
          '```\n%s\n```' % docs.format(command_prefix=self.config.command_prefix)
        )
        return

      response = yield from handler(**handler_kwargs)
      if response and isinstance(response, Response):
        content = response.content
        if response.reply:
          content = '%s, %s' % (message.author.mention, content)

        sentmsg = yield from self.send_message(message.channel, content)

        if response.delete_after > 0:
          yield from asyncio.sleep(response.delete_after)
          yield from self.delete_message(sentmsg)

    except CommandError as e:
      yield from self.send_message(message.channel, '```\n%s\n```' % e.message)

    except:
      yield from self.send_message(message.channel, '```\n%s\n```' % traceback.format_exc())
      traceback.print_exc()


if __name__ == '__main__':
  bot = Bot()
  bot.run()